
<div class="container-fluid rodape">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>


		<div class="container top20 bottom20">
			<div class="row">

				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-xs-3">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->



				<div class="col-xs-9 top20">
					<div class="barra_branca">
						<ul class="menu_rodape">
							<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
							</li>
							<li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a>
							</li>
							<li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or  Url::getURL( 0 ) == "marca"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
							</li>

							<li class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a>
							</li>
							<li  class="<?php if(Url::getURL( 0 ) == "promocoes"){ echo "active"; } ?>">
								<a href="<?php echo Util::caminho_projeto() ?>/promocoes">PROMOÇÕES</a>
							</li>
							<li class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
							</li>
						</ul>

					</div>
					<div class="clearfix">	</div>

					<div class="col-xs-11 top20 padding0">
						<!-- ======================================================================= -->
						<!-- MENU  lojas -->
						<!-- ======================================================================= -->
						<?php require_once('./includes/rodape_lojas_redes.php') ?>
						<!-- ======================================================================= -->
						<!-- MENU   lojas -->
						<!-- ======================================================================= -->
					</div>

					<div class="col-xs-1 padding0 text-right top25">
						<a href="http://www.homewebbrasil.com.br" target="_blank">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
						</a>
					</div>
					
				</div>





			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape-preto">
		<div class="col-xs-12 text-center top10 bottom10">
			<h5>© Copyright GRIFFE PNEUS</h5>
		</div>
	</div>
</div>
