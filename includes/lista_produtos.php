<?php
if(mysql_num_rows($result) == 0){
  ?>
  <div class="col-xs-12 top20 bottom20 total-resultado-busca bottom10 bg_branco">
    <h1 class="cor-preta">PARA MAIORES INFORMAÇÕES SOBRE ESSE PRODUTO ENTRE EM CONTATO COM UMA DE NOSSAS LOJAS AGORA.</h1>
  </div>
  <div class="">
    <div class="dropdown">
      <a class="btn btn_ligue input100" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        LIGUE AGORA <i class="fa fa-caret-down pull-right right10 top5" aria-hidden="true"></i>
      </a>
      <div class="dropdown-menu pull-right topo_meu_loja_prod" aria-labelledby="dropdownMenu1">
        <div class="nossa_lojas_empresa">

          <!-- ======================================================================= -->
          <!-- nossas lojas  -->
          <!-- ======================================================================= -->
          <?php
          $result = $obj_site->select("tb_categorias_unidades");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
              ++$i;
              ?>
              <div class="col-xs-4 borda div_personalizada">

                <h5><span><?php Util::imprime($row[titulo]); ?></span></h5>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.jpg" alt="">

                <?php
                $result1 = $obj_site->select("tb_unidades", "and id_categoriaunidade = $row[0] ");
                if (mysql_num_rows($result1) > 0) {
                  while($row1 = mysql_fetch_array($result1)){
                    ?>

                    <div class="media">
                      <div class="media-left media-middle">
                        <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="" />
                      </div>
                      <div class="media-body">
                        <!-- ======================================================================= -->
                        <!-- CONTATOS   -->
                        <!-- ======================================================================= -->
                        <div class="col-xs-12 padding0">
                          <h1 class="media-heading"><?php Util::imprime($row1[titulo]); ?></h1>
                        </div>
                        <div class="col-xs-8 padding0">
                          <h3 class="pull-left  top5">
                            <b><span><?php Util::imprime($row1[ddd1]) ?></span><?php Util::imprime($row1[telefone1]) ?></b>
                          </h3>

                          <?php if (!empty($row1[telefone2])): ?>
                            <h3 class="pull-right top5">
                              <span><i class="fa fa-whatsapp" aria-hidden="true"></i><?php Util::imprime($row1[ddd2]) ?></span><?php Util::imprime($row1[telefone2]) ?>
                            </h3>
                          <?php endif; ?>

                          <?php if (!empty($row1[telefone3])): ?>
                            <h3 class="pull-left top5">
                              <span><?php Util::imprime($row1[ddd3]) ?></span><?php Util::imprime($row1[telefone3]) ?>
                            </h3>
                          <?php endif; ?>

                          <?php if (!empty($row1[telefone4])): ?>
                            <h3 class="pull-right top5">
                              <span><?php Util::imprime($row1[ddd4]) ?></span><?php Util::imprime($row1[telefone4]) ?>
                            </h3>
                          <?php endif; ?>
                        </div>

                        <!-- ======================================================================= -->
                        <!-- CONTATOS   -->
                        <!-- ======================================================================= -->

                        <div class="col-xs-4 div_personalizada padding0">
                          <a href="<?php echo Util::caminho_projeto() ?>/loja/<?php Util::imprime($row1[url_amigavel]); ?>" class="btn btn_localizacao left5">CONTATO</a>
                        </div>

                      </div>

                    </div>


                    <?php
                  }
                }
                ?>

              </div>


              <?php
            }
          }
          ?>
          <!-- ======================================================================= -->
          <!-- nossas lojas  -->
          <!-- ======================================================================= -->



        </div>
      </div>
    </div>
  </div>
  <?php
}else{
  $i = 0;
  while($row = mysql_fetch_array($result))
  {
    $result1 = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
    $row1 = mysql_fetch_array($result1);
    ?>


    <!-- item 01 -->
    <div class="col-xs-4 div_personalizada">
      <div class="thumbnail produtos-home">

        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 232, 344, array("class"=>"input100 img", "alt"=>"$row[titulo]")) ?>


        <div class="produto-hover">
          <div class="col-xs-6 text-center">
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_saiba_mais.png" alt="" class="input100">
            </a>
          </div>
          <div class="col-xs-6 hover-btn-orcamento">
            <a href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_adicionar_orcamento.png" alt="" class="input100">
            </a>
          </div>
        </div>

        <div class="caption">
          <h1><?php Util::imprime($row1[titulo]); ?></h1>
          <h1><span><?php Util::imprime($row[titulo]); ?> ARO <?php Util::imprime($row[aro]); ?> - <span><?php Util::imprime($row[largura]); ?> / <?php Util::imprime($row[altura]); ?></h1>
          </div>

        </div>
      </div>
      <?php
      if ($i == 2) {
        echo '<div class="clearfix"></div>';
        $i = 0;
      }else{
        $i++;
      }
    }
  }
  ?>

  <!--  ==============================================================  -->
  <!-- produtos home -->
  <!--  ==============================================================  -->
