
<div class="container-fluid bottom20">
  <div class="row topo">

    <div class="container">
      <div class="row">

        <div class="col-xs-3 logo">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
          </a>
        </div>


        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->
        <div class="col-xs-9 padding0 menu">
          <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
          <div id="navbar">
            <ul class="nav navbar-nav padding0 lato_black">
              <li >
                <a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
                </li>

                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a>
                  </li>

                  <li >
                    <a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "orcamento" or Url::getURL( 0 ) == "marca"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
                    </li>


                    <li>
                      <a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico" or Url::getURL( 0 ) == "orcamento" ){ echo "active"; } ?>"
                        href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a>
                      </li>



                      <li >
                        <a class="<?php if(Url::getURL( 0 ) == "promocoes" or Url::getURL( 0 ) == "promocao"){ echo "active"; } ?>"
                          href="<?php echo Util::caminho_projeto() ?>/promocoes">PROMOÇÕES</a>
                        </li>



                        <li >
                          <a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
                            href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
                          </li>

                        </ul>

                      </div><!--/.nav-collapse -->

                      <!-- ======================================================================= -->
          						<!-- MENU  lojas -->
          						<!-- ======================================================================= -->
          						<?php require_once('./includes/topo_lojas.php') ?>
          						<!-- ======================================================================= -->
          						<!-- MENU   lojas -->
          						<!-- ======================================================================= -->

                      <!--  ==============================================================  -->
                      <!--CARRINHO-->
                      <!--  ==============================================================  -->
                      <div class="top15 col-xs-1">
                        <div class="dropdown">
                          <a class="btn_topo" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_ocamento.png" alt="" />
                            <!-- <span class="badge"><?php //echo count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]); ?></span> -->

                          </a>


                          <div class="dropdown-menu topo-meu-orcamento " aria-labelledby="dropdownMenu1">

                            <!--  ==============================================================  -->
                            <!--sessao adicionar produtos-->
                            <!--  ==============================================================  -->
                            <?php
                            if(count($_SESSION[solicitacoes_produtos]) > 0)
                            {
                              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                              {
                                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                                ?>
                                <div class="lista-itens-carrinho">
                                  <div class="col-xs-2">
                                    <?php if(!empty($row[imagem])): ?>
                                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                    <?php endif; ?>
                                  </div>
                                  <div class="col-xs-8">
                                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                                  </div>
                                  <div class="col-xs-1">
                                    <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                  </div>
                                  <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>
                                </div>
                                <?php
                              }
                            }
                            ?>


                            <!--  ==============================================================  -->
                            <!--sessao adicionar servicos-->
                            <!--  ==============================================================  -->
                            <?php
                            if(count($_SESSION[solicitacoes_servicos]) > 0)
                            {
                              for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                              {
                                $row = $obj_site->select_unico("tb_tipos_servicos", "idtiposervico", $_SESSION[solicitacoes_servicos][$i]);
                                ?>
                                <div class="lista-itens-carrinho">
                                  <div class="col-xs-2">
                                    <?php if(!empty($row[imagem])): ?>
                                      <img class="carrinho_servcos" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                                      <!-- <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?> -->
                                    <?php endif; ?>
                                  </div>
                                  <div class="col-xs-8">
                                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                                  </div>
                                  <div class="col-xs-1">
                                    <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                  </div>
                                  <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>

                                </div>
                                <?php
                              }
                            }
                            ?>




                            <div class="col-xs-12 text-right top10 bottom20">
                              <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn_topo" >
                                ENVIAR ORÇAMENTO
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--  ==============================================================  -->
                      <!--CARRINHO-->
                      <!--  ==============================================================  -->
                    </div>
                    <!--  ==============================================================  -->
                    <!-- MENU-->
                    <!--  ==============================================================  -->







                  </div>
                </div>

              </div>
            </div>
