<div class="dropup col-xs-5 padding0">
	<button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		<img src="<?php echo Util::caminho_projeto() ?>/imgs/nossas_lojas_rodape.png" alt="">
	</button>
	<div class="dropdown-menu  rodape_meu_loja" aria-labelledby="dropdownMenu2">
		<div class="nossa_lojas_empresa">


			<!-- ======================================================================= -->
			<!-- nossas lojas  -->
			<!-- ======================================================================= -->
			<?php
			$result = $obj_site->select("tb_categorias_unidades");
			if (mysql_num_rows($result) > 0) {
				while($row = mysql_fetch_array($result)){
					++$i;
					?>
					<div class="col-xs-4 borda div_personalizada">

						<h5><span><?php Util::imprime($row[titulo]); ?></span></h5>
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.jpg" alt="">

						<?php
						$result1 = $obj_site->select("tb_unidades", "and id_categoriaunidade = $row[0] ");
						if (mysql_num_rows($result1) > 0) {
							while($row1 = mysql_fetch_array($result1)){
								?>

								<div class="media">
									<div class="media-left media-middle">
										<a href="<?php Util::imprime($row1[src_place]); ?>" target="_blank">
										<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="" />
										</a>
									</div>
									<div class="media-body">
										<!-- ======================================================================= -->
										<!-- CONTATOS   -->
										<!-- ======================================================================= -->
										<div class="col-xs-12 padding0">
											<h1 class="media-heading"><?php Util::imprime($row1[titulo]); ?></h1>
										</div>
										<div class="col-xs-8 padding0 ">
											<h3 class="pull-left  top5">
												<b><span><?php Util::imprime($row1[ddd1]) ?></span><?php Util::imprime($row1[telefone1]) ?></b>
											</h3>

											<?php if (!empty($row1[telefone2])): ?>
												<h3 class="pull-right top5">
													<span><i class="fa fa-whatsapp" aria-hidden="true"></i><?php Util::imprime($row1[ddd2]) ?></span><?php Util::imprime($row1[telefone2]) ?>
												</h3>
											<?php endif; ?>

											<?php if (!empty($row1[telefone3])): ?>
												<h3 class="pull-left top5">
													<span><?php Util::imprime($row1[ddd3]) ?></span><?php Util::imprime($row1[telefone3]) ?>
												</h3>
											<?php endif; ?>

											<?php if (!empty($row1[telefone4])): ?>
												<h3 class="pull-right top5">
													<span><?php Util::imprime($row1[ddd4]) ?></span><?php Util::imprime($row1[telefone4]) ?>
												</h3>
											<?php endif; ?>
										</div>

										<!-- ======================================================================= -->
										<!-- CONTATOS   -->
										<!-- ======================================================================= -->

										<div class="col-xs-3 padding0">
											<a href="<?php echo Util::caminho_projeto() ?>/loja/<?php Util::imprime($row1[url_amigavel]); ?>" class="btn btn_localizacao left5">CONTATOS</a>
										</div>

									</div>

								</div>


								<?php
							}
						}
						?>

					</div>


					<?php
				}
			}
			?>
			<!-- ======================================================================= -->
			<!-- nossas lojas  -->
			<!-- ======================================================================= -->

		</div>
	</div>
</div>


<!-- ======================================================================= -->
<!-- REDES SOCIAIS    -->
<!-- ======================================================================= -->
<div class="top5 col-xs-7 padding0">


	<div class="pull-left">
		<h1>FALE COM OUVIDORIA</h1>
		<h1><span><?php Util::imprime($config[email_ouvidoria]); ?></span></h1>
		<p><?php Util::imprime($config[ddd5]); ?> <?php Util::imprime($config[telefone5]); ?></p>
	</div>
	<div class="pull-right">
		<?php if ($config[facebook] != "") { ?>
			<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
				<i class="fa fa-facebook right15 icon-rodape"></i>
			</a>
			<?php } ?>

			<?php if ($config[twitter] != "") { ?>
				<a href="<?php Util::imprime($config[twitter]); ?>" title="twitter" target="_blank" >
					<i class="fa fa-twitter right15 icon-rodape"></i>
				</a>
				<?php } ?>

				<?php if ($config[instagram] != "") { ?>
					<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
						<i class="fa fa-instagram right15 icon-rodape"></i>
					</a>
					<?php } ?>

					<?php if ($config[google_plus] != "") { ?>
						<a href="<?php Util::imprime($config[google_plus]); ?>" title="google plus" target="_blank" >
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_google_mais.png" alt="">
						</a>
						<?php } ?>
	</div>
				</div>
				<!-- ======================================================================= -->
				<!-- REDES SOCIAIS    -->
				<!-- ======================================================================= -->
