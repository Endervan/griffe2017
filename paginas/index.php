<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php if ($row[url] != ''): ?>
                    <a>
                    <?php endif; ?>

                    <div class="container">
                      <div class="row">

                        <div class="rsContent">
                          <div class="col-xs-5 col-xs-offset-2">
                            <!-- slide with any content -->

                            <table border="0" class="table" id="table-promocoes-index"  style="" >

                              <?php
                              for($temp = 1; $temp < 9; $temp++):
                                $var1 = 'frase_'.$temp;
                                $var2 = 'frase_'.$temp.'_campo_2';
                                $var3 = 'frase_'.$temp.'_campo_3';
                                $var4 = 'frase_'.$temp.'_campo_4';
                                $var5 = 'frase_'.$temp.'_campo_5';
                                ?>

                                <tr>
                                  <td class="tb-inde-medida"><?php Util::imprime($row[$var1]) ?></td>
                                  <td class="tb-inde-aro"><?php Util::imprime($row[$var2]) ?></td>
                                  <td class="tb-inde-aro"><?php Util::imprime($row[$var5]) ?></td>
                                  <td class="tb-inde-medida-2"><?php Util::imprime($row[$var3]) ?></td>
                                  <td class="tb-inde-preco">R$ <font><?php Util::imprime($row[$var4]) ?></font></td>
                                </tr>

                              <?php endfor; ?>

                            </table>

                          </div>

                          <div class="col-xs-12 top15">
                            <!-- ======================================================================= -->
                            <!-- MENU  lojas -->
                            <!-- ======================================================================= -->
                            <a class="btn-primary btn_compre" href="javascript:void(0);" data-toggle="modal" data-target="#myModal-compre">
                              COMPRE AGORA
                            </a>
                            <!-- ======================================================================= -->
                            <!-- MENU   lojas -->
                            <!-- ======================================================================= -->



                          </div>
                        </div>


                      </div>
                    </div>

                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>

                    <?php if ($row[url] != ''): ?>
                    </a>
                  <?php endif; ?>





                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- MODAL COMPRE   -->
  <!-- ======================================================================= -->
  <div class="modal fade nossa_lojas_empresa" id="myModal-compre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content drop_compre">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">ESCOLHA SUA LOJA</h4>
        </div>
        <div class="modal-body ">

          <!-- ======================================================================= -->
          <!-- nossas lojas  -->
          <!-- ======================================================================= -->
          <?php
          $result = $obj_site->select("tb_categorias_unidades");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
              ++$i;
              ?>
              <div class="col-xs-4 borda div_personalizada">

                <h5><span><?php Util::imprime($row[titulo]); ?></span></h5>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.jpg" alt="">

                <?php
                $result1 = $obj_site->select("tb_unidades", "and id_categoriaunidade = $row[0] ");
                if (mysql_num_rows($result1) > 0) {
                  while($row1 = mysql_fetch_array($result1)){
                    ?>

                    <div class="media">
                      <div class="media-left media-middle">
                        <a href="<?php Util::imprime($row1[src_place]); ?>" target="_blank">
                          <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="" />
                        </a>
                      </div>
                      <div class="media-body">
                        <!-- ======================================================================= -->
                        <!-- CONTATOS   -->
                        <!-- ======================================================================= -->
                        <div class="col-xs-12 padding0">
                          <h1 class="media-heading"><?php Util::imprime($row1[titulo]); ?></h1>
                        </div>
                        <div class="col-xs-8 padding0">
                          <h3 class="pull-left  top5">
                            <b><span><?php Util::imprime($row1[ddd1]) ?></span><?php Util::imprime($row1[telefone1]) ?></b>
                          </h3>

                          <?php if (!empty($row1[telefone2])): ?>
                            <h3 class="pull-right top5">
                              <span><i class="fa fa-whatsapp" aria-hidden="true"></i><?php Util::imprime($row1[ddd2]) ?></span><?php Util::imprime($row1[telefone2]) ?>
                            </h3>
                          <?php endif; ?>

                          <?php if (!empty($row1[telefone3])): ?>
                            <h3 class="pull-left top5">
                              <span><?php Util::imprime($row1[ddd3]) ?></span><?php Util::imprime($row1[telefone3]) ?>
                            </h3>
                          <?php endif; ?>

                          <?php if (!empty($row1[telefone4])): ?>
                            <h3 class="pull-right top5">
                              <span><?php Util::imprime($row1[ddd4]) ?></span><?php Util::imprime($row1[telefone4]) ?>
                            </h3>
                          <?php endif; ?>
                        </div>

                        <!-- ======================================================================= -->
                        <!-- CONTATOS   -->
                        <!-- ======================================================================= -->

                        <div class="col-xs-4 div_personalizada padding0">
                          <a href="<?php echo Util::caminho_projeto() ?>/loja/<?php Util::imprime($row1[url_amigavel]); ?>" class="btn btn_localizacao left5">SAIBA MAIS</a>
                        </div>

                      </div>

                    </div>


                    <?php
                  }
                }
                ?>

              </div>


              <?php
            }
          }
          ?>
          <!-- ======================================================================= -->
          <!-- nossas lojas  -->
          <!-- ======================================================================= -->



        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- MODAL COMPRE   -->
  <!-- ======================================================================= -->





  <div class="container-fluid">
    <div class="row bg_produtos_home">
      <div class="container">
        <div class="row">



          <!--  ==============================================================  -->
          <!--   barra pesquisas e categorias -->
          <!--  ==============================================================  -->
          <?php require_once("./includes/filtro_produtos.php"); ?>
          <!--  ==============================================================  -->
          <!--   barra pesquisas e categorias -->
          <!--  ==============================================================  -->




        </div>
      </div>


    </div>
  </div>


  <div class="container">
    <div class="row produtos_destaques">



      <div class="col-xs-12 padding0">

        <!--  ==============================================================  -->
        <!-- CATEGORIAS programar com 8 itens -->
        <!--  ==============================================================  -->
        <div class="col-xs-12 menu_cat top50 bottom15">

          <div id="navbar">
            <ul class="nav navbar-nav nav-left menu_categotias">

              <li class="top10"><h1 >PNEUS EM DESTAQUES PARA :</h1></li>
              <?php
              $result = $obj_site->select("tb_fabricantes");
              if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                  ?>
                  <li>
                    <a href="<?php echo Util::caminho_projeto() ?>/filtro/fabricante/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]); ?>" >
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 140, 40, array("class"=>"input100", "alt"=>"")) ?>
                    </a>
                  </li>

                  <?php
                }
              }
              ?>

            </ul>

          </div><!--/.nav-collapse -->
        </div>
        <!--  ==============================================================  -->
        <!-- CATEGORIAS -->
        <!--  ==============================================================  -->





        <!--  ==============================================================  -->
        <!-- produtos home -->
        <!--  ==============================================================  -->
        <?php
        $i = 0;
        $result = $obj_site->select("tb_categorias_produtos", "and imagem <> '' ORDER BY RAND() LIMIT 8");
        if (mysql_num_rows($result) > 0) {
          while($row = mysql_fetch_array($result)){
            ?>
            <!-- item 01 -->
            <div class="col-xs-3 div_personalizada">
              <div class="thumbnail produtos-home">

                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 232, 344, array("class"=>"input100 img", "alt"=>"$row[titulo]")) ?>


                <div class="produto-hover">
                  <div class="col-xs-6 col-xs-offset-3 text-center">
                    <a href="<?php echo Util::caminho_projeto() ?>/filtro/marca-pneu/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_saiba_mais.png" alt="" class="input100">
                    </a>
                  </div>
                </div>

                <div class="caption">
                  <h1><?php Util::imprime($row[titulo]); ?></h1>
                </div>

              </div>
            </div>
            <?php
            if ($i == 3) {
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }
          }

        }
        ?>
        <!--  ==============================================================  -->
        <!-- produtos home -->
        <!--  ==============================================================  -->

      </div>

      <div class="top30 text-center bottom80 lato_black">
        <a class="btn btn_produtos_home " href="<?php echo Util::caminho_projeto() ?>/filtro" tilte="saiba mais">ABRIR TODOS OS PRODUTOS</a>
      </div>
      <!--  ==============================================================  -->
      <!-- produto-->
      <!--  ==============================================================  -->
    </div>
  </div>








  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row bg_servicos_home">
      <div class="container">
        <div class="row ">

          <div class="col-xs-8 col-xs-offset-4 top60 empresa_titulo">
            <h1 class="pb20">NOSSOS SERVIÇOS</h1>
          </div>
          <div class="col-xs-8 col-xs-offset-4 padding0">
            <?php
            $result = $obj_site->select("tb_tipos_servicos", "limit 6");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
                //  busca a qtd de produtos cadastrados
                // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


                switch ($i) {
                  case 1:
                  $cor_class = 'DIREITO_TRIBUTÁRIO';
                  break;
                  case 2:
                  $cor_class = 'DIREITO_CIVIL';
                  break;
                  case 3:
                  $cor_class = 'DIREIT_ DO_TRABALHO';
                  break;
                  case 4:
                  $cor_class = 'DIREITO_TRIBUTÁRIO';
                  break;
                  case 5:
                  $cor_class = 'DIREITO_CIVIL';
                  break;

                  default:
                  $cor_class = 'DIREITO_DO_TRABALHO';
                  break;
                }
                $i++;
                ?>
                <!-- ======================================================================= -->
                <!--ITEM 01 REPETI ATE 6 ITENS   -->
                <!-- ======================================================================= -->
                <div class="col-xs-6  servicos pt25 pb25 <?php echo $cor_class; ?> ">

                  <div class="media">
                    <a class="media-left media-middle" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                    </a>
                    <div class="media-body">
                      <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

                        <h2 class="media-heading servico_tiulo lato_bold"><?php Util::imprime($row[titulo]); ?></h2>
                        <div class=" servicos_desc">
                          <p><?php Util::imprime($row[descricao]); ?></p>
                        </div>
                      </a>
                    </div>

                  </div>


                </div>

                <?php
              }
            }
            ?>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- SERVICOS HOME -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- EMPRESA HOME  -->
<!-- ======================================================================= -->
<div class="container top50">
  <div class="row">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
    <div class="col-xs-12 empresa_titulo">
      <h1 class="pb20"><?php Util::imprime($row[titulo]); ?></h1>
    </div>

    <div >
      <img class="input100" src="<?php echo Util::caminho_projeto() ?>/imgs/imagem-conheca_a_griffe_pneus.jpg" alt="">
    </div>

    <div class="col-xs-12">
      <div class="top10 empresa_desc_home">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
      <a class="btn btn_home_empresa pull-right bottom25" href="<?php echo Util::caminho_projeto() ?>/empresa" tilte="saiba mais">SAIBA MAIS</a>
    </div>

  </div>
</div>

<!-- ======================================================================= -->
<!-- EMPRESA HOME  -->
<!-- ======================================================================= -->






<div class="container">
  <div class="row ">
    <!-- ======================================================================= -->
    <!-- nossas lojas   -->
    <!-- ======================================================================= -->
    <div class="col-xs-5 div_personalizada borda_home">


      <div class="panel-group" id="accordion-lista-lojas" role="tablist" aria-multiselectable="true">


        <?php
        $b = 0;
        $result1 = $obj_site->select("tb_categorias_unidades");
        if (mysql_num_rows($result1) > 0) {
          while($row1 = mysql_fetch_array($result1)){
            ?>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="heading-lojas-<?php echo $b; ?>">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion-lista-lojas" href="#collapse-lojas-<?php echo $b; ?>" aria-expanded="false" aria-controls="collapse-lojas-<?php echo $b; ?>">
                    NOSSAS LOJAS EM <span class="lato_black"><?php echo Util::imprime($row1[titulo]) ?></span>
                  </a>
                </h4>
              </div>
              <div id="collapse-lojas-<?php echo $b; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-lojas-<?php echo $b; ?>">
                <div class="panel-body">
                  <div class="meu_loja_map ">
                    <!-- Nav tabs -->
                    <ul class="nav nav-pill" role="tablist">
                      <?php
                      $result = $obj_site->select("tb_unidades","and id_categoriaunidade = '$row1[0]' ");
                      if (mysql_num_rows($result) > 0) {
                        $i = 0;
                        while($row = mysql_fetch_array($result)){
                          $unidades[] = $row;
                          ?>

                          <div class="media">
                            <a class="media-left media-middle">
                              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="" />
                            </a>
                            <div class="media-body">
                              <!-- ======================================================================= -->
                              <!-- CONTATOS   -->
                              <!-- ======================================================================= -->
                              <h3 class="col-xs-12 padding0 media-heading"><?php Util::imprime($row[endereco]); ?></h3>
                              <div class="col-xs-5 padding0">
                                <h3 class="top5 font20">
                                  <i class="fa fa-phone right5" aria-hidden="true"></i>
                                  <b><span><?php Util::imprime($row[ddd1]) ?></span><?php Util::imprime($row[telefone1]) ?></b>
                                </h3>

                                <?php if (!empty($row[telefone2])): ?>
                                  <h3 class="top10">
                                    <i class="fa fa-whatsapp right5" aria-hidden="true"></i>
                                    <span><?php Util::imprime($row[ddd2]) ?></span><?php Util::imprime($row[telefone2]) ?>
                                  </h3>
                                <?php endif; ?>

                                <?php if (!empty($row[telefone3])): ?>
                                  <h3 class="top10">
                                    <i class="fa fa-phone right5" aria-hidden="true"></i>
                                    <span><?php Util::imprime($row[ddd3]) ?></span><?php Util::imprime($row[telefone3]) ?>
                                  </h3>
                                <?php endif; ?>

                                <?php if (!empty($row[telefone4])): ?>
                                  <h3 class="top10">
                                    <i class="fa fa-phone right5" aria-hidden="true"></i>
                                    <span><?php Util::imprime($row[ddd4]) ?></span><?php Util::imprime($row[telefone4]) ?>
                                  </h3>
                                <?php endif; ?>
                              </div>


                              <!-- ======================================================================= -->
                              <!-- CONTATOS   -->
                              <!-- ======================================================================= -->
                              <div class="col-xs-4 col-xs-offset-3 padding0" role="presentation" class="<?php if($i == 0){ echo 'active'; } ?>">
                                <a href="#tab_<?php Util::imprime($row[idunidade]); ?>" aria-controls="<?php echo $b; ?>" role="tab" data-toggle="tab" class="btn btn_localizacao">ABRIR NO MAPA</a>
                              </div>

                            </div>

                          </div>



                          <?php
                          $i++;
                        }
                      }
                      ?>
                    </ul>

                  </div>
                </div>
              </div>
            </div>
            <?php
            $b++;
          }
        }
        ?>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- nossas lojas   -->
    <!-- ======================================================================= -->

    <div class="col-xs-7 padding0 empresa_titulo">

      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
      <!-- Tab panes -->
      <div class="tab-content ">
        <?php
        if (count($unidades) > 0) {
          $i = 0;
          foreach ($unidades as $key => $row) {
            ?>
            <div role="tabpanel" class="tab-pane <?php if($i == 0){ echo 'active'; } ?>" id="tab_<?php Util::imprime($row[idunidade]); ?>">
              <iframe src="<?php Util::imprime($row[src_place]); ?>" width="100%" height="447" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <?php
            $i++;
          }
        }
        ?>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>

    </div>
  </div>
</div>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>

<?php require_once('./includes/js_css.php') ?>
