<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  82px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <div class="container">
    <div class="row ">
      <!--  ==============================================================  -->
      <!--  MENU lateral-->
      <!--  ==============================================================  -->
      <div class="col-xs-3 top35 bg_branco pt20 bottom50">
        <?php require_once("./includes/menu_produtos.php") ?>
      </div>
      <!--  ==============================================================  -->
      <!--  MENU lateral-->
      <!--  ==============================================================  -->


      <div class="col-xs-9 padding0">

        <!-- ======================================================================= -->
        <!-- PRODUTOS DESCRICAO  -->
        <!-- ======================================================================= -->
        <div class="servico_geral desc_prod">
          <h1 class="left15 lato_black"><span >NOSSOS PRODUTOS</span></h1>
          <h5 class="left15 lato_black">ENCONTRE O PRODUTO IDEAL:</h5>
        </div>
        <!-- ======================================================================= -->
        <!-- PRODUTOS DESCRICAO  -->
        <!-- ======================================================================= -->




        <form method="post" action="<?php echo Util::caminho_projeto() ?>/filtro/" >





          <!--  ==============================================================  -->
          <!-- BARRA MODELOS ORDENA POR MODELO-->
          <!--  ==============================================================  -->
          <div class="col-xs-4 pt0 top15">
            <select name="topo_marca_veiculos" id="topo_marca_veiculos" class="select-topo input100 btn-busca-prod">
              <option value="">MARCA</option>
              <?php
              $result = $obj_site->select("tb_marcas_veiculos", "and id_tipoveiculo = 1");
              if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                  ?>
                  <option value="<?php Util::imprime($row[0]); ?>" data-tokens="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?></option>
                  <?php
                }
              }
              ?>
            </select>
          </div>
          <!--  ==============================================================  -->
          <!-- BARRA MODELOS ORDENA POR MODELO-->
          <!--  ==============================================================  -->


          <!--  ==============================================================  -->
          <!-- BARRA CATEGORIA ORDENA POR MODELO-->
          <!--  ==============================================================  -->
          <div class="col-xs-4 pt0 top15">
            <select name="topo_modelo_veiculos" id="topo_modelo_veiculos" class="select-topo input100 btn-busca-prod">
              <option value="">MODELO</option>
            </select>
          </div>
          <!--  ==============================================================  -->
          <!-- BARRA CATEGORIA ORDENA POR MODELO-->
          <!--  ==============================================================  -->



          <div class="col-xs-3 busca top15">
            <div class="input-group input-group-lg">
              <span class="input-group-btn">
                <button class="btn btn-drop btn-busca-prod"name="busca_topo"  value="OK" type="submit"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
        </form>


        <!--  ==============================================================  -->
        <!-- produto home-->
        <!--  ==============================================================  -->
        <div class="clearfix"></div>
        <div class="top35 padding0 bottom30">

            <?php $result = $obj_site->select("tb_categorias_produtos"); ?>




          <?php
         // TESTANDO SELECIONANDO  >>> MINI >>> ONE
         //  busca os produtos sem filtro
         $result = $obj_site->select("tb_produtos", $complemento);

         //  ====================================================================== //
         //  filto via get
         //  ====================================================================== //


         //  FILTRA PELA MARCA
         $url1 = Url::getURL(1);
         $filtro = Url::getURL(2);

         if($url1 != ''){
           if($url1 == 'veiculo'){
             if (isset( $url1 )) {
               $id_categoria = $obj_site->get_id_url_amigavel("tb_tipos_veiculos", "idtipoveiculo", $filtro );
               $complemento .= "AND id_tipoveiculo = '$id_categoria' ";
             }
           }

           if($url1 == 'marca-pneu'){
             if (isset( $url1 )) {
               $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $filtro );
               $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
             }
           }


           if($url1 == 'aro'){
             if (isset( $url1 )) {
               $complemento .= "AND aro = '$filtro' ";
             }
           }

           if($url1 == 'medida'){
             if (isset( $url1 )) {
               $larg = explode("-",$filtro);
               $complemento .= "AND largura = '$larg[0]' AND altura = '$larg[1]' ";
             }
           }

           if($url1 == 'fabricante'){
             if (isset( $url1 )) {
               $id_fabricante = $obj_site->get_id_url_amigavel("tb_fabricantes", "idfabricante", $filtro );
               $complemento .= "AND id_fabricante = '$id_fabricante' ";
             }
           }

           $result = $obj_site->select("tb_produtos", $complemento);
         }

         //  ====================================================================== //
         //  filto via get
         //  ====================================================================== //



            ?>
            <div class="col-xs-12 total-resultado-busca bottom10">
              <h1><span><?php echo mysql_num_rows($result) ?></span> PRODUTO(S) ENCONTRADO(S).</h1>
            </div>
            <?php
            //  LISTA OS PRODUTOS
            require_once("./includes/lista_produtos.php");
          
            ?>



        </div>
        <!--  ==============================================================  -->
        <!-- produto home-->
        <!--  ==============================================================  -->


      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
