<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
  //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
    case "piscina_vinil":
    $id = $_GET[id];
    unset($_SESSION[piscina_vinil][$id]);
    sort($_SESSION[piscina_vinil]);
    break;
  }

}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- TRABALHE TITULO  -->
  <!-- ======================================================================= -->
  <div class="container top30">
    <div class="row">
      <div class="col-xs-6 servico_geral">
        <h1>ENVIE<span class="clearfix">SEU ORÇAMENTO</span></h1>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TRABALHE TITULO  -->
  <!-- ======================================================================= -->


  <div class="container-fluid container_fundo_servico">
    <div class="row">
      <div class="content_fundo_servico_dentro"> </div>

      <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
        <div class="container bottom40">
          <div class="row ">

            <div class="col-xs-8 padding0">

              <!-- ======================================================================= -->
              <!-- CARRINHO  -->
              <!-- ======================================================================= -->
              <div class="col-xs-6 tabela_carrinho pt25">
                  <?php if (!empty($_SESSION[solicitacoes_produtos])): ?>
                <h3 class="top20">ITENS SELECIONADOS (PRODUTOS)</h3>
                <table class="table table-condensed">
                  <tbody>

                    <?php
                    if(count($_SESSION[solicitacoes_produtos]) > 0){
                      for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                        $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                        ?>
                        <tr>
                          <td>
                            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 60, 49, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                          </td>
                          <td class="col-xs-12"><?php Util::imprime($row[titulo]); ?></td>
                          <td class="text-center">
                            QTD.<br>
                            <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                            <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                          </td>
                          <td class="text-center">
                            <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                              <i class="fa fa-times-circle fa-2x top20"></i>
                            </a>
                          </td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                  </tbody>
                </table>
              <?php endif; ?>
              </div>

              <div class="col-xs-6 tabela_carrinho pt25">
                <?php if (!empty($_SESSION[solicitacoes_servicos])): ?>
                <h3 class="top20">ITENS SELECIONADOS (SERVIÇOS)</h3>
                <table class="table table-condensed">
                  <tbody>


                    <?php
                    if(count($_SESSION[solicitacoes_servicos]) > 0){
                      for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
                        $row = $obj_site->select_unico("tb_tipos_servicos", "idtiposervico", $_SESSION[solicitacoes_servicos][$i]);
                        ?>
                        <tr>
                          <td>
                            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                            <!-- <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 60, 49, array("class"=>"", "alt"=>"$row[titulo]")) ?> -->
                          </td>
                          <td class="col-xs-12"><?php Util::imprime($row[titulo]); ?></td>
                          <td class="text-center">
                            QTD.<br>
                            <input type="text" class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                            <input name="idtiposervico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                          </td>
                          <td class="text-center">
                            <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                              <i class="fa fa-times-circle fa-2x top20"></i>
                            </a>
                          </td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                  </tbody>
                </table>
              <?php endif; ?>
              </div>
              <!-- ======================================================================= -->
              <!-- CARRINHO  -->
              <!-- ======================================================================= -->

              <div class="col-xs-12 padding0">

                <div class="fundo_formulario">
                  <h3 class="top20 left15">CONFIRME SEUS DADOS</h3>
                  <!-- formulario orcamento -->

                  <div class="col-xs-12 top20">
                    <div class="form-group input100 has-feedback">
                      <select name="email_loja" id="email_loja" class="form-control fundo-form1 input100 input-lg">
                          <option value="">SELECIONE A LOJA DESEJADA</option>
                          <?php
                          $i = 0;
                          $sql = "
                                    SELECT
                                        tcu.titulo as cidade , tu.*
                                    FROM
                                        tb_unidades tu, tb_categorias_unidades tcu
                                    WHERE
                                        tu.id_categoriaunidade = tcu.idcategoriaunidade
                                    ORDER BY
                                        tcu.titulo, tu.titulo
                                 ";
                          $result = $obj_site->executaSQL($sql);
                          if (mysql_num_rows($result) > 0) {
                            while($row = mysql_fetch_array($result)){
                                $nome_loja = $row[cidade] . " - " . $row[titulo];
                                if (!empty($row[email])) {
                                ?>
                                <option value="<?php echo Util::imprime($row[email]) ?>"><?php echo Util::imprime($nome_loja) ?></option>
                                <?php
                                }
                              }
                              $i++;
                          }
                          ?>
                      </select>
                      <span class="fa fa-user form-control-feedback"></span>
                    </div>
                  </div>



                  <div class="clearfix"></div>

                  <div class="top20">

                    <div class="col-xs-6">
                      <div class="form-group input100 has-feedback">
                        <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                        <span class="fa fa-user form-control-feedback top15"></span>
                      </div>
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group  input100 has-feedback">
                        <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                        <span class="fa fa-envelope form-control-feedback top15"></span>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="top20">
                    <div class="col-xs-6">
                      <div class="form-group  input100 has-feedback">
                        <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                        <span class="fa fa-phone form-control-feedback top15"></span>
                      </div>
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group  input100 has-feedback">
                        <input type="text" name="localidade" class="form-control fundo-form1 input-lg input100" placeholder="LOCALIDADE">
                        <span class="fa fa-home form-control-feedback"></span>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="top15">
                    <div class="col-xs-12">
                      <div class="form-group input100 has-feedback">
                        <textarea name="mensagem" cols="30" rows="6" class="form-control fundo-form1 input100" placeholder="OBSERVAÇÕES"></textarea>
                        <span class="fa fa-pencil form-control-feedback top15"></span>
                      </div>
                    </div>
                  </div>

                  <!-- formulario orcamento -->
                  <div class="col-xs-12 text-right">
                    <div class="text-left top15">
                        <span >*campo obrigatório</span>
                    </div>
                    <div class="">
                      <button type="submit" class="btn btn_formulario1" name="btn_contato">
                        ENVIAR
                      </button>
                    </div>
                  </div>

                </div>
                <!--  ==============================================================  -->
                <!-- FORMULARIO-->
                <!--  ==============================================================  -->
              </div>


            </div>
          </div>
        </div>

      </div>
    </div>

  </form>





  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->




<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_POST[nome])){

  //  CADASTRO OS PRODUTOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd]); $i++){
    $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd][$i] ."</p></td>
    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
    </tr>
    ";
  }

  //  CADASTRO OS SERVICOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd_servico]); $i++){
    $dados = $obj_site->select_unico("tb_tipos_servicos", "idtiposervico", $_POST[idtiposervico][$i]);

    $servicos .= "
    <tr>
    <td><p>". $_POST[qtd_servico][$i] ."</p></td>
    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
    </tr>
    ";
  }




  //  ENVIANDO A MENSAGEM PARA O CLIENTE
  $texto_mensagem = "
  O seguinte cliente fez uma solicitação pelo site. <br />

  Nome: $_POST[nome] <br />
  Email: $_POST[email] <br />
  Telefone: $_POST[telefone] <br />
  Localidade: $_POST[localidade] <br />
  Mensagem: <br />
  ". nl2br($_POST[mensagem]) ." <br />

  <br />
  <h2> Produtos selecionados:</h2> <br />

  <table width='100%' border='0' cellpadding='5' cellspacing='5'>
  <tr>
  <td><h4>QTD</h4></td>
  <td><h4>PRODUTO</h4></td>
  </tr>
  $produtos
  $servicos
  </table>

  ";



  Util::envia_email($_POST[email_loja], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

  unset($_SESSION[solicitacoes_produtos]);
  unset($_SESSION[solicitacoes_servicos]);
  unset($_SESSION[piscinas_vinil]);
  Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

}
?>





<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu nome'

          }
        }
      },

      email_loja: {
        validators: {
          notEmpty: {
            message: 'Por favor selecione a loja desejada'

          }
        }
      },

      email: {
        validators: {
          notEmpty: {
            message: 'insira seu email'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Informe um telefone'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      localidade1: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem1: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
