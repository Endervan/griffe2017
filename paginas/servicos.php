<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  82px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- SERVICOS TITULO  -->
  <!-- ======================================================================= -->
  <div class="container top90">
    <div class="row">
      <div class="col-xs-5 servico_geral">
        <h1>CONFIRA <span class="clearfix">NOSSOS SERVIÇOS</span></h1>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SERVICOS TITULO  -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->

  <div class="container-fluid container_fundo_servico bottom280">
    <div class="row ">
      <div class="content_fundo_servico">  </div>

      <div class="container ">
        <div class="row top30">
          <div class="col-xs-8 padding0">
            <?php
            $result = $obj_site->select("tb_tipos_servicos");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
                //  busca a qtd de produtos cadastrados
                // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


                switch ($i) {
                  case 1:
                  $cor_class = 'BALANCEAMENTO';
                  break;
                  case 2:
                  $cor_class = 'RODIZIO';
                  break;
                  case 3:
                  $cor_class = 'SUSPENSÃO';
                  break;
                  case 4:
                  $cor_class = 'REPARO';
                  break;
                  case 5:
                  $cor_class = 'ALIANHAMENTO';
                  break;

                  default:
                  $cor_class = 'CALIBRAGEM';
                  break;
                }
                $i++;
                ?>
                <!-- ======================================================================= -->
                <!--ITEM 01 REPETI ATE 6 ITENS   -->
                <!-- ======================================================================= -->
                <div class="col-xs-6  servicos pt20 pb20 <?php echo $cor_class; ?> ">

                  <div class="media">
                    <a class="media-left media-middle" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />

                    <div class="media-body">

                        <h2 class="media-heading servico_tiulo"><?php Util::imprime($row[titulo]); ?></h2>
                        <div class=" servicos_desc">
                          <p><?php Util::imprime($row[descricao]); ?></p>
                        </div>
                      </a>
                    </div>

                  </div>


                </div>

                <?php
              }
            }
            ?>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- SERVICOS HOME -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
