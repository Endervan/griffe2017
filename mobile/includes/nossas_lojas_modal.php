<!-- Large modal -->
<div class="text-center">
        <button type="button" class="btn btn_localizacao" data-toggle="modal" data-target=".bs-example-modal-lg">CONHEÇA NOSSAS LOJAS</button>
</div>


<div class="modal fade bs-example-modal-lg modal-nossas-lojas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
		<!-- ======================================================================= -->
		<!-- nossas lojas  -->
		<!-- ======================================================================= -->
		<?php
		$result = $obj_site->select("tb_categorias_unidades");
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_array($result)){
				++$i;
				?>
				<div class="col-xs-12 borda div_personalizada">

					<h5><span><?php Util::imprime($row[titulo]); ?></span></h5>
					<img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.jpg" alt="">

					<?php
					$result1 = $obj_site->select("tb_unidades", "and id_categoriaunidade = $row[0] ");
					if (mysql_num_rows($result1) > 0) {
						while($row1 = mysql_fetch_array($result1)){
							?>

							<div class="media left30">
								<div class="media-left media-middle">
									<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="" />
								</div>
								<div class="media-body">
									<!-- ======================================================================= -->
									<!-- CONTATOS   -->
									<!-- ======================================================================= -->
									<div class="col-xs-12 padding0">
										<h1 class="media-heading"><?php Util::imprime($row1[titulo]); ?></h1>
									</div>
									<div class="col-xs-8 padding0">
										<h3 class="pull-left top5">
											<?php Util::imprime($row1[ddd1]) ?></span><?php Util::imprime($row1[telefone1]) ?>
										</h3>

										<?php if (!empty($row1[telefone2])): ?>
											<h3 class="pull-right top5">
												<span><i class="fa fa-whatsapp" aria-hidden="true"></i><?php Util::imprime($row1[ddd2]) ?></span><?php Util::imprime($row1[telefone2]) ?>
											</h3>
										<?php endif; ?>

										<?php if (!empty($row1[telefone3])): ?>
											<h3 class="pull-left top5">
												<span><?php Util::imprime($row1[ddd3]) ?></span><?php Util::imprime($row1[telefone3]) ?>
											</h3>
										<?php endif; ?>

										<?php if (!empty($row1[telefone4])): ?>
											<h3 class="pull-right top5">
												<span><?php Util::imprime($row1[ddd4]) ?></span><?php Util::imprime($row1[telefone4]) ?>
											</h3>
										<?php endif; ?>
									</div>

									<!-- ======================================================================= -->
									<!-- CONTATOS   -->
									<!-- ======================================================================= -->

									<div class="col-xs-4 div_personalizada padding0">
										<a href="<?php echo Util::caminho_projeto() ?>/loja/<?php Util::imprime($row1[url_amigavel]); ?>" class="btn btn_localizacao left5">SAIBA MAIS</a>
									</div>

								</div>

								<div class="clearfix">

								</div>

							</div>


							<?php
						}
						}
						?>

						</div>


				<?php
			}
		}
		?>
		<!-- ======================================================================= -->
		<!-- nossas lojas  -->
		<!-- ======================================================================= -->
    </div>
  </div>
</div>
