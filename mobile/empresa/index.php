
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 62px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- EMPRESA DESCRICAO  -->
	<!-- ======================================================================= -->
	<div class="container top60">
		<div class="row">
			<div class="col-xs-12 empresa_geral">
				<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
				<h5 class="pb20"><?php Util::imprime($row[titulo]); ?></h5>
				<div  class="desc_empresa_geral">
					<p><?php Util::imprime($row[descricao]); ?></p>
				</div>
			</div>
		</div>
	</div>

	<!-- ======================================================================= -->
	<!-- EMPRESA DESCRICAO  -->
	<!-- ======================================================================= -->




	<div class="container">
		<div class="row empresa_geral">

			<!-- ======================================================================= -->
			<!-- MISSÃO  -->
			<!-- ======================================================================= -->
			<div class="col-xs-12 ">
				<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
				<div class="top30">
					<h5><?php Util::imprime($row[titulo]); ?></h5>
				</div>
				<div class="top15 desc_empresa_missao">
					<p><?php Util::imprime($row[descricao]); ?></p>
				</div>

				<!-- ======================================================================= -->
				<!-- MISSÃO  -->
				<!-- ======================================================================= -->
			</div>

			<div class="col-xs-6">
				<!-- ======================================================================= -->
				<!-- visão  -->
				<!-- ======================================================================= -->

				<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
				<div class="top30">
					<h5><?php Util::imprime($row[titulo]); ?></h5>
				</div>
				<div class="top15 desc_empresa_visao">
					<p><?php Util::imprime($row[descricao]); ?></p>
				</div>

				<!-- ======================================================================= -->
				<!-- visão  -->
				<!-- ======================================================================= -->


			</div>



		</div>
	</div>



	<div class="container">
		<div class="row nossa_lojas_empresa bottom50">
			<div class="col-xs-12 top35 bottom55 lato_black">
				<h6>NOSSAS LOJAS</h6>
			</div>
			<!-- ======================================================================= -->
			<!-- nossas lojas  brasilia -->
			<!-- ======================================================================= -->
			<?php
			$i = 0;
			$result = $obj_site->select("tb_categorias_unidades");
			if (mysql_num_rows($result) > 0) {
				while($row = mysql_fetch_array($result)){
					?>
					<div class="col-xs-12 borda top10 div_personalizada <?php if($i == 2){ echo 'top200'; } ?> ">

						<h5><span><?php Util::imprime($row[titulo]); ?></span></h5>
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.jpg" alt="">

						<?php
						$result1 = $obj_site->select("tb_unidades", "and id_categoriaunidade = $row[0] ");
						if (mysql_num_rows($result1) > 0) {
							while($row1 = mysql_fetch_array($result1)){
								?>

								<div class="media">
									<div class="media-left media-middle">
										<a class="media-left media-middle" href="<?php Util::imprime($row1[src_place]); ?>" target="_blank">
											<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="" />
										</a>
									</div>
									<div class="media-body">
										<!-- ======================================================================= -->
										<!-- CONTATOS   -->
										<!-- ======================================================================= -->
										<div class="col-xs-12 padding0">
											<h1 class="media-heading"><?php Util::imprime($row1[endereco]); ?></h1>
										</div>
										<div class="col-xs-8 padding0">
											<a href="tel:+55<?php Util::imprime($row1[ddd1]) ?><?php Util::imprime($row1[telefone1]) ?>">

												<h3 class="pull-left  top5">
													<b><span><?php Util::imprime($row1[ddd1]) ?></span><?php Util::imprime($row1[telefone1]) ?></b>
												</h3>
											</a>

											<?php if (!empty($row1[telefone2])): ?>
												<a href="tel:+55<?php Util::imprime($row1[ddd2]) ?><?php Util::imprime($row1[telefone2]) ?>">
													<h3 class="pull-right top5">
														<span><?php Util::imprime($row1[ddd2]) ?></span><?php Util::imprime($row1[telefone2]) ?><i class="fa fa-whatsapp" aria-hidden="true"></i>
													</h3>
												</a>
											<?php endif; ?>

											<?php if (!empty($row1[telefone3])): ?>
												<a href="tel:+55<?php Util::imprime($row1[ddd3]) ?><?php Util::imprime($row1[telefone3]) ?>">
													<h3 class="pull-left top5">
														<span><?php Util::imprime($row1[ddd3]) ?></span><?php Util::imprime($row1[telefone3]) ?>
													</h3>
												</a>
											<?php endif; ?>

											<?php if (!empty($row1[telefone4])): ?>
												<a href="tel:+55<?php Util::imprime($row1[ddd4]) ?><?php Util::imprime($row1[telefone4]) ?>">
													<h3 class="pull-right top5">
														<span><?php Util::imprime($row1[ddd4]) ?></span><?php Util::imprime($row1[telefone4]) ?><i class="fa fa-whatsapp" aria-hidden="true"></i>
													</h3>
												</a>
											<?php endif; ?>

										</div>

										<!-- ======================================================================= -->
										<!-- CONTATOS   -->
										<!-- ======================================================================= -->


										<div class="col-xs-3 padding0">
											<a href="<?php echo Util::caminho_projeto() ?>/mobile/loja/<?php Util::imprime($row1[url_amigavel]); ?>" class="btn btn_localizacao left5">CONTATOS</a>
										</div>

									</div>

								</div>


								<?php
							}
						}
						?>

					</div>


					<?php
					++$i;
				}
			}
			?>
			<!-- ======================================================================= -->
			<!-- nossas lojas  brasilia -->
			<!-- ======================================================================= -->



		</div>
	</div>



	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>




<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
