
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",16); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 63px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->



	<div class="container">
		<div class="row top70 busca-interna-produtos">

			<div class="col-xs-12">
					<h1>NOSSOS PRODUTOS</h1>
			</div>


			<!--  ==============================================================  -->
			<!--   barra pesquisas e categorias -->
			<!--  ==============================================================  -->
			<?php require_once("../includes/filtro_produtos.php"); ?>
			<!--  ==============================================================  -->
			<!--   barra pesquisas e categorias -->
			<!--  ==============================================================  -->
		</div>
	</div>





	<!--  ==============================================================  -->
	<!--  FILTROS CATEGORIAS -->
	<!--  ==============================================================  -->
	<div class="container">
		<div class="row top20">
			<div class="col-xs-12">


				<!-- Button trigger modal -->
				<button type="button" class="btn btn-amarelo input100" data-toggle="modal" data-target="#myModal_filtro_marca">
				  FILTRAR POR MARCA
				</button>

				<!-- Modal -->
				<div class="modal fade" id="myModal_filtro_marca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Selecione a opção desejada</h4>
					  </div>
					  <div class="modal-body">
						  <?php require_once("../includes/filtro_marcas.php"); ?>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					  </div>
					</div>
				  </div>
				</div>





			</div>
		</div>
	</div>
	<!--  ==============================================================  -->
	<!--  FILTROS CATEGORIAS -->
	<!--  ==============================================================  -->




	<div class="container">
			<div class="row">

				<!--  ==============================================================  -->
				<!-- produtos home -->
				<!--  ==============================================================  -->
				<?php
				//  busca os produtos sem filtro
				$result = $obj_site->select("tb_produtos", $complemento);


				//  ====================================================================== //
				//  filto via get
				//  ====================================================================== //
				$get = explode("/", $_GET[get1]);


				//  FILTRA PELA MARCA
				$url1 = $get[0];
				$filtro = $get[1];

				if($url1 != ''){
						if($url1 == 'marca-pneu'){
							if (isset( $url1 )) {
								$id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $filtro );
				                $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
							}
						}


						if($url1 == 'aro'){
							if (isset( $url1 )) {
								$complemento .= "AND aro = '$filtro' ";
							}
						}

						if($url1 == 'medida'){
							if (isset( $url1 )) {
								$larg = explode("-",$filtro);
								$complemento .= "AND largura = '$larg[0]' AND altura = '$larg[1]' ";
							}
						}


						if($url1 == 'fabricante'){
			              if (isset( $url1 )) {
			                $id_fabricante = $obj_site->get_id_url_amigavel("tb_fabricantes", "idfabricante", $filtro );
			                $complemento .= "AND id_fabricante = '$id_fabricante' ";
			              }
			            }

						$result = $obj_site->select("tb_produtos", $complemento);
				}


				//  ====================================================================== //
				//  filto via get
				//  ====================================================================== //


				//  ====================================================================== //
	            //  filto via post
	            //  ====================================================================== //
	            if(isset($_POST[topo_tipos_veiculos]) or isset($_POST[topo_marca_veiculos]) ){
	              //  tipo de veiculo
	              if(isset($_POST[topo_tipos_veiculos]) and $_POST[topo_tipos_veiculos] != '' ){
	                $complemento .= "AND tp.id_tipoveiculo = '$_POST[topo_tipos_veiculos]' ";
	              }

	              //  marca
	              if(isset($_POST[topo_marca_veiculos]) and $_POST[topo_marca_veiculos] != '' ){
	                $complemento .= "AND tpma.id_marcaveiculo = '$_POST[topo_marca_veiculos]' ";
	              }


	              //  modelo
	              if(isset($_POST[topo_modelo_veiculos]) and $_POST[topo_modelo_veiculos] != '' ){
	                $complemento .= "AND tpma.id_modeloveiculo = '$_POST[topo_modelo_veiculos]' ";
	              }


	              $sql = "
	                      SELECT
	                          tp.*
	                      FROM
	                          tb_produtos_modelos_aceitos tpma, tb_produtos tp
	                      WHERE
	                          tp.idproduto = tpma.id_produto
	                          AND tp.ativo = 'SIM'
	                          $complemento
	                      GROUP BY
	                          tp.titulo
	                      ";
	              $result = $obj_site->executaSQL($sql);
	            }
	            //  ====================================================================== //
	            //  filto via post
	            //  ====================================================================== //

				?>




				<div class="container">
						<div class="row">
								<div class="col-xs-12 total-resultado-busca top15">
										<h1><span><?php echo mysql_num_rows($result) ?></span> PRODUTO(S) ENCONTRADO(S).</h1>
								</div>
						</div>
				</div>




				<?php
				if(mysql_num_rows($result) == 0){
					?>
					<div class="container">
						<div class="row">
							<div class="col-xs-12 total-resultado-busca bottom10 bg_branco">
						      <h1 class="cor-preta top20 bottom20">PARA MAIORES INFORMAÇÕES SOBRE ESSE PRODUTO ENTRE EM CONTATO COM UMA DE NOSSAS LOJAS AGORA.</h1>
						      <div class="col-xs-12 bottom40">
						          <?php require_once("../includes/nossas_lojas_modal.php") ?>
						      </div>
						    </div>
						</div>
					</div>

					<?php
				}else{
					while($row = mysql_fetch_array($result))
					{
						$result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
						$row1 = mysql_fetch_array($result_categoria);
						?>
						<!-- item 01 -->
						<div class="col-xs-6 top20">
							<div class="thumbnail produtos-home">
								<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
									<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 210, 290, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
								</a>

								<div class="caption">
									<a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
										<h1><?php Util::imprime($row1[titulo]); ?></h1>
										<h1><span><?php Util::imprime($row[titulo],200); ?> ARO <?php Util::imprime($row[aro],200); ?> - <span><?php Util::imprime($row[medida],200); ?></h1>
										</a>
									</div>

								</div>
							</div>
							<?php
						}
					}
					?>
					<!--  ==============================================================  -->
					<!-- produtos home -->
					<!--  ==============================================================  -->

			</div>
	</div>











	<div class="clearfix">	</div>
	<div class="top90">	</div>


	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>




<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
