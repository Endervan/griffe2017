<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>
</head>

<body>




  <!-- ======================================================================= -->
  <!-- slider	-->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 2");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
              /*
              ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
              <?php
              */
              $i++;
            }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">

                <?php if (!empty($imagem[url])) { ?>
                  <a >
                    <?php } ?>

                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                    <div class="carousel-caption col-xs-offset-2">
                      <table border="0" class="table" id="table-promocoes-index"  style="" >
                        <?php
                        for($temp = 1; $temp < 9; $temp++):
                          $var1 = 'frase_'.$temp;
                          $var2 = 'frase_'.$temp.'_campo_2';
                          $var3 = 'frase_'.$temp.'_campo_3';
                          $var4 = 'frase_'.$temp.'_campo_4';
                          $var5 = 'frase_'.$temp.'_campo_5';
                          ?>
                          <tr>
                            <td class="tb-inde-medida"><?php Util::imprime($imagem[$var1]) ?></td>
                            <td class="tb-inde-aro"><?php Util::imprime($imagem[$var2]) ?></td>
                            <td class="tb-inde-aro"><?php Util::imprime($imagem[$var5]) ?></td>
                            <td class="tb-inde-medida-2"><?php Util::imprime($imagem[$var3]) ?></td>
                            <td class="tb-inde-preco">R$ <font><?php Util::imprime($imagem[$var4]) ?></font></td>
                          </tr>
                        <?php endfor; ?>
                      </table>

                    <div class="col-xs-12 left5 text-right">
                      <a style="padding: 11px; background: #004e98; border: none; margin-top: -2px;" class="btn btn-primary btn_compre" href="javascript:void(0);" data-toggle="modal" data-target="#myModal-lojas-topo">
                        COMPRE AGORA
                      </a>
                    </div>

                    </div>
                    <?php if (!empty($imagem[url])) { ?>
                    </a>
                    <?php } ?>

                  </div>

                  <?php
                  $i++;
                }
              }
              ?>




            </div>


            <?php /* ?>
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
            <?php */ ?>



          </div>
        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- slider	-->
      <!-- ======================================================================= -->


      <!-- ======================================================================= -->
      <!-- topo	-->
      <!-- ======================================================================= -->
      <div class="topo-site">
        <?php require_once('./includes/topo.php'); ?>
      </div>
      <!-- ======================================================================= -->
      <!-- topo	-->
      <!-- ======================================================================= -->

      <div class="container">
        <div class="row bg_produtos_home">
          <!--  ==============================================================  -->
          <!--   barra pesquisas e categorias -->
          <!--  ==============================================================  -->
          <?php require_once("./includes/filtro_produtos.php"); ?>
          <!--  ==============================================================  -->
          <!--   barra pesquisas e categorias -->
          <!--  ==============================================================  -->
        </div>
      </div>



      <div class="container">
        <div class="row produtos_destaques">
          <!--  ==============================================================  -->
          <!-- CATEGORIAS programar com 8 itens -->
          <!--  ==============================================================  -->
          <div class="col-xs-12 menu_cat top15 bottom15">

            <div id="navbar">
              <div class="top10 col-xs-12 padding0"><h1>PNEUS EM DESTAQUES PARA :</h1></div>
              <ul class=" menu_categotias">


                <?php
                $result = $obj_site->select("tb_fabricantes");
                if (mysql_num_rows($result) > 0) {
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <li class="col-xs-4  top20">
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/filtro/fabricante/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]); ?>" >
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 500, 150, array("class"=>"input100", "alt"=>"")) ?>
                      </a>
                    </li>

                    <?php
                  }
                }
                ?>

              </ul>

            </div>
          </div>
          <!--  ==============================================================  -->
          <!-- CATEGORIAS -->
          <!--  ==============================================================  -->

        </div>
      </div>


      <div class="container ">
        <div class="row">

          <!--  ==============================================================  -->
          <!-- produtos home -->
          <!--  ==============================================================  -->

          <?php
          $result = $obj_site->select("tb_categorias_produtos", "and imagem <> '' order by rand() limit 4");
          if(mysql_num_rows($result) > 0)
          {
            while($row = mysql_fetch_array($result))
            {
              ?>
              <!-- item 01 -->
              <div class="col-xs-6 top20">
                <div class="thumbnail produtos-home">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/filtro/marca-pneu/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 210, 290, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                  </a>

                  <div class="caption">
                    <a href="<?php echo Util::caminho_projeto() ?>/filtro/marca-pneu/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <h1><?php Util::imprime($row[titulo]); ?></h1>
                    </a>
                  </div>

                </div>
              </div>
              <?php
            }
          }
          ?>
          <!--  ==============================================================  -->
          <!-- produtos home -->
          <!--  ==============================================================  -->

          <div class="col-xs-12 top35 text-center lato_black">
            <a class="btn btn_produtos_home " href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" tilte="saiba mais">ABRIR TODOS OS PRODUTOS</a>
          </div>
        </div>
      </div>



      <!-- ======================================================================= -->
      <!-- SERVICOS HOME -->
      <!-- ======================================================================= -->
      <div class="container top40">
        <div class="row bg_servicos_home">

          <div class="col-xs-10 col-xs-offset-2 top70 empresa_titulo">
            <h1 class="pb20">NOSSOS SERVIÇOS</h1>
          </div>
          <div class="col-xs-10 col-xs-offset-2 padding0">
            <?php
            $result = $obj_site->select("tb_tipos_servicos", "limit 6");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
                //  busca a qtd de produtos cadastrados
                // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


                switch ($i) {
                  case 1:
                  $cor_class = 'DIREITO_TRIBUTÁRIO';
                  break;
                  case 2:
                  $cor_class = 'DIREITO_CIVIL';
                  break;
                  case 3:
                  $cor_class = 'DIREIT_ DO_TRABALHO';
                  break;
                  case 4:
                  $cor_class = 'DIREITO_TRIBUTÁRIO';
                  break;
                  case 5:
                  $cor_class = 'DIREITO_CIVIL';
                  break;

                  default:
                  $cor_class = 'DIREITO_DO_TRABALHO';
                  break;
                }
                $i++;
                ?>
                <!-- ======================================================================= -->
                <!--ITEM 01 REPETI ATE 6 ITENS   -->
                <!-- ======================================================================= -->
                <div class="col-xs-12  servicos pt20 pb20 <?php echo $cor_class; ?> ">

                  <div class="media">
                    <a class="media-left media-middle" href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />

                      <div class="media-body">

                        <h2 class="media-heading servico_tiulo"><?php Util::imprime($row[titulo]); ?></h2>
                        <div class=" servicos_desc">
                          <p><?php Util::imprime($row[descricao]); ?></p>
                        </div>
                      </a>
                    </div>

                  </div>


                </div>

                <?php
              }
            }
            ?>
          </div>

        </div>
      </div>

      <!-- ======================================================================= -->
      <!-- SERVICOS HOME -->
      <!-- ======================================================================= -->


      <!-- ======================================================================= -->
      <!-- EMPRESA HOME  -->
      <!-- ======================================================================= -->
      <div class="container top40">
        <div class="row">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
          <div class="col-xs-12 empresa_titulo">
            <h1 class="pb20"><?php Util::imprime($row[titulo]); ?></h1>
          </div>

          <div class="col-xs-12 empresa_desc">
            <div class="top15">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>

        </div>
      </div>

      <!-- ======================================================================= -->
      <!-- EMPRESA HOME  -->
      <!-- ======================================================================= -->


      <div class="container">
        <div class="row ">
          <!-- ======================================================================= -->
          <!-- nossas lojas   -->
          <!-- ======================================================================= -->
          <div class="col-xs-12">


            <a class="btn_topo" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
              <img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/nossas_lojas_home.png" alt="" />
            </a>

            <div class="collapse" id="collapseExample">
              <div class="meu_loja_map">
                <!-- Nav tabs -->
                <ul class="nav nav-pill" role="tablist">
                  <?php
                  $result = $obj_site->select("tb_categorias_unidades","ORDER BY titulo");
                  if (mysql_num_rows($result) > 0) {
                    while($row = mysql_fetch_array($result)){
                      ++$i;
                      ?>
                      <h5><span><?php Util::imprime($row[titulo]); ?></span></h5>
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.jpg" alt="">

                      <?php
                      $result1 = $obj_site->select("tb_unidades", "and id_categoriaunidade = $row[0] ");
                      if (mysql_num_rows($result1) > 0) {
                        $i = 0;
                        while($row1 = mysql_fetch_array($result1)){
                          $unidades[] = $row1;
                          ?>

                          <div class="media">
                            <a class="media-left media-middle" href="<?php Util::imprime($row1[src_place]); ?>" target="_blank">
                              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="" />
                            </a>
                            <div class="media-body">
                              <!-- ======================================================================= -->
                              <!-- CONTATOS   -->
                              <!-- ======================================================================= -->
                              <h3 class="col-xs-12 padding0 media-heading"><?php Util::imprime($row1[titulo]); ?></h3>
                              <div class="col-xs-5 padding0">
                                <a href="tel:+55<?php Util::imprime($row1[ddd1]) ?><?php Util::imprime($row1[telefone1]) ?>">
                                  <h3 class="telefone_bold">
                                    <b><span><?php Util::imprime($row1[ddd1]) ?></span><?php Util::imprime($row1[telefone1]) ?></b>
                                  </h3>
                                </a>

                                <?php if (!empty($row1[telefone2])): ?>
                                  <a href="tel:+55<?php Util::imprime($row1[ddd2]) ?><?php Util::imprime($row1[telefone2]) ?>">
                                    <h3 class="top10">
                                      <span><?php Util::imprime($row1[ddd2]) ?></span><?php Util::imprime($row1[telefone2]) ?><i class="fa fa-whatsapp left5" aria-hidden="true"></i>
                                    </h3>
                                  </a>
                                <?php endif; ?>

                                <?php if (!empty($row1[telefone3])): ?>
                                  <a href="tel:+55<?php Util::imprime($row1[ddd3]) ?><?php Util::imprime($row1[telefone3]) ?>">
                                    <h3 class="top10">
                                      <span><?php Util::imprime($row1[ddd3]) ?></span><?php Util::imprime($row1[telefone3]) ?><i class="fa fa-whatsapp left5" aria-hidden="true"></i>
                                    </h3>
                                  </a>
                                <?php endif; ?>

                                <?php if (!empty($row1[telefone4])): ?>
                                  <a href="tel:+55<?php Util::imprime($row1[ddd4]) ?><?php Util::imprime($row1[telefone4]) ?>">
                                    <h3 class="top10">
                                      <span><?php Util::imprime($row1[ddd4]) ?></span><?php Util::imprime($row1[telefone4]) ?><i class="fa fa-whatsapp left5" aria-hidden="true"></i>
                                    </h3>
                                  </a>
                                <?php endif; ?>
                              </div>


                              <!-- ======================================================================= -->
                              <!-- CONTATOS   -->
                              <!-- ======================================================================= -->
                              <div class="col-xs-7 padding0" role="presentation" class="<?php if($i == 0){ echo 'active'; } ?>">
                                <a onclick="$('html,body').animate({scrollTop: $('.home_toque').offset().top}, 2000);" href="#tab_<?php Util::imprime($row1[idunidade]); ?>" aria-controls="home" role="tab" data-toggle="tab" class="btn btn_localizacao">ABRIR NO MAPA</a>
                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/loja/<?php Util::imprime($row1[url_amigavel]); ?>" class="btn btn_localizacao left5">SAIBA MAIS</a>
                              </div>

                            </div>
                          </div>
                          <?php
                          $i++;
                        }
                      }
                      ?>

                    </ul>


                    <?php
                  }
                }
                ?>

              </div>
            </div>

          </div>
          <!-- ======================================================================= -->
          <!-- nossas lojas   -->
          <!-- ======================================================================= -->

          <div class="col-xs-12 top15">
            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
            <!-- Tab panes -->
            <div class="home_toque"></div>
            <div class="tab-content">
              <?php
              if (count($unidades) > 0) {
                $i = 0;
                foreach ($unidades as $key => $row1) {
                  ?>
                  <div role="tabpanel" class="tab-pane <?php if($i == 0){ echo 'active'; } ?>" id="tab_<?php Util::imprime($row1[idunidade]); ?>">
                    <iframe src="<?php Util::imprime($row1[src_place]); ?>" width="100%" height="544" frameborder="0" style="border:0" allowfullscreen></iframe>
                  </div>
                  <?php
                  $i++;
                }
              }
              ?>
              <!-- ======================================================================= -->
              <!-- mapa   -->
              <!-- ======================================================================= -->
            </div>
          </div>

        </div>
      </div>



      <!-- ======================================================================= -->
      <!-- modal    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/modal.php') ?>
      <!-- ======================================================================= -->
      <!-- modal    -->
      <!-- ======================================================================= -->

      <!-- ======================================================================= -->
      <!-- modal    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/rodape.php') ?>
      <!-- ======================================================================= -->
      <!-- modal    -->
      <!-- ======================================================================= -->

    </body>

    </html>





    <?php require_once('./includes/js_css.php') ?>
