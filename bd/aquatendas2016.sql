-- phpMyAdmin SQL Dump
-- version 4.4.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 10-Fev-2016 às 22:13
-- Versão do servidor: 5.6.25
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aquatendas2016`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_avaliacoes_produtos`
--

INSERT INTO `tb_avaliacoes_produtos` (`idavaliacaoproduto`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_produto`, `data`, `nota`) VALUES
(26, 'Marcio', 'marciomas@gmail.com', 'Um excelente produto, recomendo a todos.', 'SIM', 0, ' ', 7, '2016-01-28', 5),
(28, ' ', ' ', ' ', 'NAO', 0, ' ', 7, '2016-01-28', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desCategoria_Dica_Modelcricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `desCategoria_Dica_Modelcricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(57, 'PISCINAS', NULL, '2701201602021288111244.png', 'SIM', NULL, 'piscinas', NULL, NULL, NULL),
(59, 'ACESSÓRIOS', NULL, '2701201602041208570733.png', 'SIM', NULL, 'acessorios', NULL, NULL, NULL),
(60, 'TENDAS', NULL, '2701201602041311380045.png', 'SIM', NULL, 'tendas', NULL, NULL, NULL),
(61, 'FILTROS E BOMBAS', NULL, '2701201602041273827844.png', 'SIM', NULL, 'filtros-e-bombas', NULL, NULL, NULL),
(62, 'PRODUTOS QUÍMICOS', NULL, '2701201602041291193930.png', 'SIM', NULL, 'produtos-quimicos', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_comentarios_dicas`
--

INSERT INTO `tb_comentarios_dicas` (`idcomentariodica`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_dica`, `data`) VALUES
(6, 'André', 'andre@masmidia.com.br', 'Testando o comentário.', 'SIM', 0, ' ', 41, '2016-02-01'),
(7, 'Joana', 'joana@masmidia.com.br', 'Aqui fica o comentário do cliente.', 'SIM', 0, ' ', 41, '2016-02-01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_piscinas_vinil`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_piscinas_vinil` (
  `idcomentariopiscinavinil` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_piscinavinil` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_comentarios_piscinas_vinil`
--

INSERT INTO `tb_comentarios_piscinas_vinil` (`idcomentariopiscinavinil`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_piscinavinil`, `data`, `nota`) VALUES
(29, 'Maria Aparecida', 'maria@masmidia.com.br', 'Uma excelente piscina, adorei a instalação.', 'SIM', 0, ' ', 0, '2016-02-10', 5),
(30, ' ', ' ', ' ', 'NAO', 0, ' ', 0, '2016-02-10', 0),
(31, ' ', ' ', ' ', 'NAO', 0, ' ', 0, '2016-02-10', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE IF NOT EXISTS `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`) VALUES
(1, 'Aquatendas 2016', '', '', '', 'SIM', 0, '', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante,Brasília - DF', '(61)3456-0987', '(61)3456-2222', '', 'https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d245795.5082910491!2d-47.93735785!3d-15.72176215!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1453991657890', NULL, NULL, NULL, '(61)3456-3333', '(61)3456-4444');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE IF NOT EXISTS `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `depoimento` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `depoimento`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(4, 'Marcos Paulo', '<p>\r\n	Muito bom lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy te xt ever since the 1500s</p>', 'SIM', NULL, 'marcos-paulo', '2801201602531377835472..jpg'),
(5, 'Ana Pedrosa', '<p>\r\n	Aqui fica lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy te xt ever since the 1500s</p>', 'SIM', NULL, 'ana-pedrosa', '2801201602531118983184..jpg'),
(6, 'Lucina Torres', '<p>\r\n	Adoreio lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy te xt ever since the 1500s</p>', 'SIM', NULL, 'lucina-torres', '2801201602531120559981..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(38, 'Cuidar da piscina - Dicas para manter a piscina sempre linda', '<p>\r\n	Os benef&iacute;cios de ter uma piscina em casa s&atilde;o enormes. Acordar cedo e dar um mergulho &eacute; uma vantagem que muitos querem e nem todos podem. E apesar de ser um enorme e irrecus&aacute;vel privil&eacute;gio, ter uma piscina em casa tamb&eacute;m nos traz algumas responsabilidades.<br />\r\n	Pode-se imaginar que a piscina &eacute; como um carro: Se voc&ecirc; n&atilde;o cuidar, acaba dando problema, e se n&atilde;o tratar de resolver o problema pode acabar o perdendo. Nesta linha de racioc&iacute;nio, manter a piscina sempre limpa &eacute; algo que deve ser feito constantemente, n&atilde;o importando o tipo da piscina e muito menos se ela est&aacute; ou n&atilde;o em uso.<br />\r\n	Na realidade existem uma maneira de interromper o uso da piscina, &eacute; o que chamamos de hiberna&ccedil;&atilde;o da piscina, mas at&eacute; isto demanda um certo cuidado. Confira no texto: &ldquo;Interrompendo o uso da piscina &ndash; Como hibernar a piscina&ldquo;.<br />\r\n	Hoje vamos dar a voc&ecirc; algumas dicas para cuidar da piscina e evitar que os problemas apare&ccedil;am. Vamos l&aacute;?</p>\r\n<p>\r\n	&nbsp;</p>', '3001201601491383238219..jpg', 'SIM', NULL, 'cuidar-da-piscina--dicas-para-manter-a-piscina-sempre-linda', NULL, NULL, NULL, NULL),
(39, 'Dicas para a manutenção de piscinas - 9 passos', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '3001201601491236205540..jpg', 'SIM', NULL, 'dicas-para-a-manutencao-de-piscinas--9-passos', NULL, NULL, NULL, NULL),
(40, 'Verão está no fim, mas manutenção de piscina precisa', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '3001201601501243792975..jpg', 'SIM', NULL, 'verao-esta-no-fim-mas-manutencao-de-piscina-precisa', NULL, NULL, NULL, NULL),
(41, 'Como limpar piscina: dicas práticas', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '3001201601501331852483..jpg', 'SIM', NULL, 'como-limpar-piscina-dicas-praticas', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Serviços - Legenda', 'Legenda serviços dolor sit amet, consectetur adipisicing elit', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(2, 'Produtos - Legenda', 'Legenda produtos dolor sit amet, consectetur adipisicing elit', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(3, 'Empresa - Legenda', 'Legenda empresa dolor sit amet, consectetur adipisicing elit', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(4, 'Empresa - Sobre', 'Sobre a empresa lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy te xt ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survi ved not only five centuries, but also the leap into electronic types etting, remaining essentially unchanged. It was popularised in the', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(5, 'Empresa - Qualidade dos serviços', 'Qualida dos serviços lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy te xt ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survi ved not only five centuries, but also the leap into electronic types etting, remaining essentially unchanged. It was popularised in the', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(6, 'Portfólio - Legenda', 'Legenda portfólio dolor sit amet, consectetur adipisicing elit', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(7, 'Dicas - Legenda', 'Legenda dicas dolor sit amet, consectetur adipisicing elit', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(8, 'Index - Conheça mais a Aquatendas', 'Conheça mais Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown pr inter took a galley of type and scrambled it to make a type specimen book. It has survive d not only five centuries, but also the leap into electronic typesetting, remaining essenti ally unchanged. It was popularised in the 1960s with', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(9, 'Orçamento - Legenda', 'Legenda orçamento dolor sit amet, consectetur adipisicing elit', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(10, 'Fale conosco - Legenda', 'Legenda fale dolor sit amet, consectetur adipisicing elit', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(11, 'Trabalhe conosco - Legenda', 'Legenda trabalhe dolor sit amet, consectetur adipisicing elit', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(12, 'Piscinal de vinil - Descrição', 'Piscina de vinil descricao Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text e\rver since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not \ronly five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text e\rver since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not \ronly five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the ', 'SIM', 0, '', '', '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(56, '2801201603021200738207.jpg', 'SIM', NULL, NULL, 7),
(57, '2801201603021232320941.jpg', 'SIM', NULL, NULL, 7),
(58, '2801201603021116541054.jpg', 'SIM', NULL, NULL, 7),
(59, '2801201603021384773400.jpg', 'SIM', NULL, NULL, 7),
(60, '2801201603021211824913.jpg', 'SIM', NULL, NULL, 7),
(61, '2801201603031133705918.jpg', 'SIM', NULL, NULL, 8),
(62, '2801201603031191229581.jpg', 'SIM', NULL, NULL, 8),
(63, '2801201603031253263167.jpg', 'SIM', NULL, NULL, 8),
(64, '2801201603031277500028.jpg', 'SIM', NULL, NULL, 8),
(65, '2801201603031365426813.jpg', 'SIM', NULL, NULL, 8),
(66, '2801201603031408059643.jpeg', 'SIM', NULL, NULL, 9),
(67, '2801201603031164317371.jpg', 'SIM', NULL, NULL, 9),
(68, '2801201603031348094918.jpg', 'SIM', NULL, NULL, 9),
(69, '2801201603031273527546.jpg', 'SIM', NULL, NULL, 9),
(70, '2801201603031136693175.jpg', 'SIM', NULL, NULL, 9),
(71, '2801201603031157967284.jpg', 'SIM', NULL, NULL, 9),
(72, '2801201603031354125587.jpg', 'SIM', NULL, NULL, 9),
(73, '2801201603031395310957.jpeg', 'SIM', NULL, NULL, 9),
(74, '2801201603031403609508.jpg', 'SIM', NULL, NULL, 9),
(75, '2801201603031138290044.jpg', 'SIM', NULL, NULL, 10),
(76, '2801201603031358200589.jpg', 'SIM', NULL, NULL, 10),
(77, '2801201603031258047521.jpg', 'SIM', NULL, NULL, 10),
(78, '2801201603031149065834.jpg', 'SIM', NULL, NULL, 10),
(79, '2801201603031115735593.jpg', 'SIM', NULL, NULL, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(99, '2701201609031139553698.jpg', 'SIM', NULL, NULL, 7),
(100, '2701201609031235763748.jpg', 'SIM', NULL, NULL, 7),
(101, '2701201609031210247888.jpg', 'SIM', NULL, NULL, 7),
(102, '2701201609031215016841.jpg', 'SIM', NULL, NULL, 7),
(103, '2701201609031141727620.jpg', 'SIM', NULL, NULL, 7),
(104, '2701201609031338499681.jpg', 'SIM', NULL, NULL, 7),
(105, '2701201609031284145870.jpeg', 'SIM', NULL, NULL, 7),
(106, '2701201609031239558345.jpg', 'SIM', NULL, NULL, 7),
(107, '2701201609031261764641.jpg', 'SIM', NULL, NULL, 7),
(108, '2701201609031244929330.jpg', 'SIM', NULL, NULL, 7),
(109, '2701201609031231402881.jpg', 'SIM', NULL, NULL, 7),
(110, '2701201609031282587834.jpg', 'SIM', NULL, NULL, 7),
(111, '2701201609031315682373.jpg', 'SIM', NULL, NULL, 7),
(112, '3001201601191360098989.jpg', 'SIM', NULL, NULL, 8),
(113, '3001201601191222063073.jpg', 'SIM', NULL, NULL, 8),
(114, '3001201601191193718058.jpg', 'SIM', NULL, NULL, 8),
(115, '3001201601191124391193.jpg', 'SIM', NULL, NULL, 8),
(116, '3001201601191400371318.jpg', 'SIM', NULL, NULL, 8),
(117, '3001201601191214055504.jpg', 'SIM', NULL, NULL, 9),
(118, '3001201601191236217598.jpg', 'SIM', NULL, NULL, 8),
(119, '3001201601191341843935.jpg', 'SIM', NULL, NULL, 9),
(120, '3001201601191168940458.jpg', 'SIM', NULL, NULL, 9),
(121, '3001201601191357083211.jpeg', 'SIM', NULL, NULL, 8),
(122, '3001201601191233765494.jpg', 'SIM', NULL, NULL, 10),
(123, '3001201601191297781748.jpg', 'SIM', NULL, NULL, 9),
(124, '3001201601191148517699.jpg', 'SIM', NULL, NULL, 8),
(125, '3001201601191160962360.jpg', 'SIM', NULL, NULL, 10),
(126, '3001201601191253916754.jpg', 'SIM', NULL, NULL, 8),
(127, '3001201601191351635807.jpg', 'SIM', NULL, NULL, 9),
(128, '3001201601191136701397.jpg', 'SIM', NULL, NULL, 10),
(129, '3001201601191322837782.jpg', 'SIM', NULL, NULL, 11),
(130, '3001201601191162313664.jpg', 'SIM', NULL, NULL, 8),
(131, '3001201601191313262517.jpg', 'SIM', NULL, NULL, 9),
(132, '3001201601191162790526.jpg', 'SIM', NULL, NULL, 11),
(133, '3001201601191207586978.jpg', 'SIM', NULL, NULL, 10),
(134, '3001201601191352457628.jpeg', 'SIM', NULL, NULL, 9),
(135, '3001201601191156202864.jpg', 'SIM', NULL, NULL, 8),
(136, '3001201601191166822943.jpg', 'SIM', NULL, NULL, 11),
(137, '3001201601191122130538.jpg', 'SIM', NULL, NULL, 13),
(138, '3001201601191349379587.jpg', 'SIM', NULL, NULL, 10),
(139, '3001201601191249791874.jpg', 'SIM', NULL, NULL, 9),
(140, '3001201601191365477267.jpg', 'SIM', NULL, NULL, 11),
(141, '3001201601191294122013.jpg', 'SIM', NULL, NULL, 13),
(142, '3001201601191292556909.jpg', 'SIM', NULL, NULL, 10),
(143, '3001201601191166364178.jpg', 'SIM', NULL, NULL, 9),
(144, '3001201601191233851071.jpg', 'SIM', NULL, NULL, 14),
(145, '3001201601191342302330.jpg', 'SIM', NULL, NULL, 14),
(146, '3001201601191358402802.jpg', 'SIM', NULL, NULL, 11),
(147, '3001201601191265550482.jpg', 'SIM', NULL, NULL, 14),
(148, '3001201601191210643985.jpg', 'SIM', NULL, NULL, 13),
(149, '3001201601191230650628.jpeg', 'SIM', NULL, NULL, 10),
(150, '3001201601191252940794.jpg', 'SIM', NULL, NULL, 9),
(151, '3001201601191306533651.jpg', 'SIM', NULL, NULL, 13),
(152, '3001201601191224895962.jpg', 'SIM', NULL, NULL, 11),
(153, '3001201601191354907616.jpg', 'SIM', NULL, NULL, 14),
(154, '3001201601191264962432.jpg', 'SIM', NULL, NULL, 9),
(155, '3001201601191235843651.jpg', 'SIM', NULL, NULL, 10),
(156, '3001201601191251708206.jpeg', 'SIM', NULL, NULL, 11),
(157, '3001201601191170300225.jpg', 'SIM', NULL, NULL, 11),
(158, '3001201601191286320779.jpg', 'SIM', NULL, NULL, 13),
(159, '3001201601191281052149.jpg', 'SIM', NULL, NULL, 13),
(160, '3001201601191355153957.jpeg', 'SIM', NULL, NULL, 13),
(161, '3001201601191248685511.jpg', 'SIM', NULL, NULL, 14),
(162, '3001201601191157348888.jpg', 'SIM', NULL, NULL, 14),
(163, '3001201601191389238416.jpeg', 'SIM', NULL, NULL, 14),
(164, '3001201601191351647201.jpg', 'SIM', NULL, NULL, 14),
(165, '3001201601191312698447.jpg', 'SIM', NULL, NULL, 14),
(166, '3001201601191339271998.jpg', 'SIM', NULL, NULL, 14);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`) VALUES
(1, 'HomeWeb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1705 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1634, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:02:53', 7),
(1635, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:03:53', 7),
(1636, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:04:03', 7),
(1637, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:04:13', 7),
(1638, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:04:26', 7),
(1639, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:04:39', 7),
(1640, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-27', '14:30:30', 7),
(1641, 'EXCLUSÃO DO LOGIN 56, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''56''', '2016-01-27', '14:30:55', 7),
(1642, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:33:45', 7),
(1643, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:33:53', 7),
(1644, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:00', 7),
(1645, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:06', 7),
(1646, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:15', 7),
(1647, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:25', 7),
(1648, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:31', 7),
(1649, 'DESATIVOU O LOGIN 63', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''63''', '2016-01-27', '14:47:12', 7),
(1650, 'DESATIVOU O LOGIN 64', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''64''', '2016-01-27', '14:47:32', 7),
(1651, 'DESATIVOU O LOGIN 69', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''69''', '2016-01-27', '14:47:35', 7),
(1652, 'DESATIVOU O LOGIN 68', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''68''', '2016-01-27', '14:47:38', 7),
(1653, 'DESATIVOU O LOGIN 67', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''67''', '2016-01-27', '14:47:42', 7),
(1654, 'DESATIVOU O LOGIN 66', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''66''', '2016-01-27', '14:47:45', 7),
(1655, 'DESATIVOU O LOGIN 65', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''65''', '2016-01-27', '14:47:49', 7),
(1656, 'EXCLUSÃO DO LOGIN 63, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''63''', '2016-01-27', '20:58:13', 7),
(1657, 'EXCLUSÃO DO LOGIN 69, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''69''', '2016-01-27', '20:58:19', 7),
(1658, 'EXCLUSÃO DO LOGIN 64, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''64''', '2016-01-27', '20:58:22', 7),
(1659, 'EXCLUSÃO DO LOGIN 68, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''68''', '2016-01-27', '20:58:25', 7),
(1660, 'EXCLUSÃO DO LOGIN 67, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''67''', '2016-01-27', '20:58:27', 7),
(1661, 'EXCLUSÃO DO LOGIN 66, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''66''', '2016-01-27', '20:58:30', 7),
(1662, 'EXCLUSÃO DO LOGIN 65, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''65''', '2016-01-27', '20:58:32', 7),
(1663, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '21:02:15', 7),
(1664, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '14:04:36', 7),
(1665, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '14:04:54', 7),
(1666, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '14:05:21', 7),
(1667, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:50:50', 7),
(1668, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:00', 7),
(1669, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:10', 7),
(1670, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:30', 7),
(1671, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:47', 7),
(1672, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:54', 7),
(1673, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:53:22', 7),
(1674, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:53:34', 7),
(1675, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:53:45', 7),
(1676, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '15:00:14', 7),
(1677, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '15:00:28', 7),
(1678, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '15:00:43', 7),
(1679, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '15:00:57', 7),
(1680, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:16:28', 7),
(1681, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:16:49', 7),
(1682, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:17:17', 7),
(1683, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:17:41', 7),
(1684, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:18:08', 7),
(1685, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:18:43', 7),
(1686, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:19:09', 7),
(1687, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:47:15', 7),
(1688, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:47:37', 7),
(1689, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:47:59', 7),
(1690, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:48:21', 7),
(1691, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-30', '13:49:49', 7),
(1692, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-30', '13:49:59', 7),
(1693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-30', '13:50:07', 7),
(1694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-30', '13:50:22', 7),
(1695, 'EXCLUSÃO DO LOGIN 58, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''58''', '2016-02-10', '14:23:32', 1),
(1696, 'CADASTRO DO CLIENTE ', '', '2016-02-10', '19:21:09', 1),
(1697, 'CADASTRO DO CLIENTE ', '', '2016-02-10', '19:21:49', 1),
(1698, 'CADASTRO DO CLIENTE ', '', '2016-02-10', '19:22:04', 1),
(1699, 'CADASTRO DO CLIENTE ', '', '2016-02-10', '19:22:13', 1),
(1700, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:41:52', 1),
(1701, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:42:14', 1),
(1702, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:42:25', 1),
(1703, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:42:36', 1),
(1704, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:42:47', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_piscinas_vinil`
--

CREATE TABLE IF NOT EXISTS `tb_piscinas_vinil` (
  `idpiscinavinil` int(10) unsigned NOT NULL,
  `titulo` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `preco` double NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem1` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_piscinas_vinil`
--

INSERT INTO `tb_piscinas_vinil` (`idpiscinavinil`, `titulo`, `imagem`, `preco`, `descricao`, `id_categoriaproduto`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `codigo`, `marca`, `url_amigavel`, `imagem1`) VALUES
(73, 'Vinil 1', '1002201607211399881042..jpg', 0, '', 0, 'SIM', 0, '', '', '', NULL, NULL, 'vinil-1', '1002201607421142154557..jpg'),
(74, 'Vinil 2', '1002201607211316302897..jpg', 0, '', 0, 'SIM', 0, '', '', '', NULL, NULL, 'vinil-2', '1002201607421384541279..jpg'),
(75, 'Vinil 3', '1002201607221174866422..jpg', 0, '', 0, 'SIM', 0, '', '', '', NULL, NULL, 'vinil-3', '1002201607421338781597..jpg'),
(76, 'Vinil 4', '1002201607221170211183..jpg', 0, '', 0, 'SIM', 0, '', '', '', NULL, NULL, 'vinil-4', '1002201607421167985008..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portifolios`
--

CREATE TABLE IF NOT EXISTS `tb_portifolios` (
  `idportifolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_portifolios`
--

INSERT INTO `tb_portifolios` (`idportifolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(7, 'Portfólio 1', '2801201603001231950844..jpg', '<p>\r\n	Descri&ccedil;&atilde;o aqui&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-1'),
(8, 'Portfólio 2', '2801201603001392287771..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-2'),
(9, 'Portfólio 3', '2801201603001158711652..jpg', '<p>\r\n	3&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-3'),
(10, 'Portfólio 5', '2801201603001168013040..jpg', '<p>\r\n	5&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-5');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`) VALUES
(7, 'Acessório 1', '2701201609021396421728..jpg', '<p>\r\n	1&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'acessorio-1', 59, 'Sandel', NULL, NULL),
(8, 'Piscina 1 lorem', '3001201601161375566029..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscina-1-lorem', 57, 'Alpha', NULL, NULL),
(9, 'Piscina de Alvenaria', '3001201601161300623723..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscina-de-alvenaria', 57, 'Alve+', NULL, NULL),
(10, 'Piscina 3', '3001201601171312375011..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscina-3', 58, 'Max', NULL, NULL),
(11, 'Piscina 4', '3001201601171208375001..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscina-4', 57, 'Unix', NULL, NULL),
(12, 'Piscina 6', '3001201601181266460670..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscina-6', 57, 'KMY', NULL, NULL),
(13, 'Piscina de Vinil 2', '3001201601181214275650..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscina-de-vinil-2', 58, 'VRX', NULL, NULL),
(14, 'Piscina Média', '3001201601191369068783..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscina-media', 57, 'POL', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`) VALUES
(37, 'INSTALAÇÃO DE PISCINAS', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '2801201602041225579006..jpg', 'SIM', NULL, 'instalacao-de-piscinas', '', '', '', 0, '', ''),
(38, 'MANUTENÇÃO DE PISCINAS', '<div>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</div>', '2801201602041259910792..jpg', 'SIM', NULL, 'manutencao-de-piscinas', '', '', '', 0, '', ''),
(39, 'LIMPEZA DE PISCINAS', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '2801201602051257444057..jpg', 'SIM', NULL, 'limpeza-de-piscinas', '', '', '', 0, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_comentarios_piscinas_vinil`
--
ALTER TABLE `tb_comentarios_piscinas_vinil`
  ADD PRIMARY KEY (`idcomentariopiscinavinil`);

--
-- Indexes for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_piscinas_vinil`
--
ALTER TABLE `tb_piscinas_vinil`
  ADD PRIMARY KEY (`idpiscinavinil`);

--
-- Indexes for table `tb_portifolios`
--
ALTER TABLE `tb_portifolios`
  ADD PRIMARY KEY (`idportifolio`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_comentarios_piscinas_vinil`
--
ALTER TABLE `tb_comentarios_piscinas_vinil`
  MODIFY `idcomentariopiscinavinil` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1705;
--
-- AUTO_INCREMENT for table `tb_piscinas_vinil`
--
ALTER TABLE `tb_piscinas_vinil`
  MODIFY `idpiscinavinil` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `tb_portifolios`
--
ALTER TABLE `tb_portifolios`
  MODIFY `idportifolio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
